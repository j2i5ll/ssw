var express = require('express');
var router = express.Router();
var fs = require("fs");
var db = require("../modules/db");

var multer = require("multer");
var upload = multer({dest:__dirname+"/../public/album"});
var noticeImg = multer({dest:__dirname+"/../public/noticeImg"});
var profilePhoto = multer({dest:__dirname+"/../public/profileImg"});
var easyimage = require("easyimage");


var bodyParser = require("body-parser");


/* GET home page. */
router.post("/logout", function(req, res, next){
	var sess = req.session;
	sess.admin = undefined;

	res.json({result : "logout"});
	
});
router.post("/login", function(req, res, next){
	var id = req.body.id;
	var pw = req.body.pw;

	var sql="select count(*) as count from admin where id = :id and pw = password(:pw)"
	
	db.pool.query(sql, 
		{
			id : id,
			pw : pw
		},
		function(err, results){

		if(err){
			throw err;
		}else{
			if(results.rows[0].count == 0){
				res.json({
					result:"fail"
				});
			}else if(results.rows[0].count == 1){
				var sess = req.session;
				sess.admin = "admin";
				res.json({result: "success"});
			}
		}
	});

});
router.delete("/gallery/:num", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}

	var num = req.params.num;
	db.pool.query("delete from gallery where num = :num",
		{ num : num},
	function(err, results){

		if(err){
			throw err;
		}else{
			res.json({ result:"success" });
		}
	});

});
router.post('/gallery/upload', upload.single("file"), function(req, res, next) {

	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}

	
	var ext = "";
	if(req.file.mimetype == "image/png"){
		ext = ".png";
	}else if(req.file.mimetype == "image/jpeg"){
		ext = ".jpg";
	}
	var newPath = req.file.path+ext ;

	function saveImage(fileInfo){
		
		console.log(fileInfo.desc);
		db.pool.query("insert into gallery(path, w, h, upload_date, description) values(:path,:w,:h, now(), :desc)",
			{
				path:fileInfo.filename,
				w:fileInfo.w,
				h:fileInfo.h,
				desc:fileInfo.desc
			},
		function(err, results){

			if(err){
				throw err;
			}else{
				res.json(results.row);
			}
		});
	}

	fs.rename(req.file.path, newPath, function(err){
		if(err){
			throw err;
		}

		var fileInfo = {
			filename: newPath.replace(/^.*[\\\/]/, ''),
			desc : req.body.desc
		}
		console.log(fileInfo.desc);
		easyimage.exec("convert "+newPath+" -print '%w %h' /dev/null")
		.then(function(res){

			var size = res.split(" ");
			fileInfo.w = parseInt(size[0]);
			fileInfo.h = parseInt(size[1]);


			var dim = undefined;

			if(fileInfo.w > fileInfo.h){
				if(fileInfo.w > 1280){
					fileInfo.h = Math.ceil((1280/fileInfo.w) * fileInfo.h);
					fileInfo.w = 1280;
					dim = fileInfo.w+"x"+fileInfo.h;
				}

			}else{
				if(fileInfo.h > 900){
					dim = "x900";
					fileInfo.w = Math.ceil((900/fileInfo.h) * fileInfo.w);
					fileInfo.h = 900;
					dim = fileInfo.w+"x"+fileInfo.h;

				}
			}
			if(dim){
				return easyimage.exec("convert "+newPath+" -resize "+dim+" "+newPath);
				/*
				return easyimage.resize({
					src : newPath,
					dst : newPath,
					width:newW,
					height:newH

				});
				*/
				
				//saveImage(filename);
			}

		})
		.then(function(file){
			saveImage(fileInfo);
		});
	});

});
router.put("/program/lec/:num", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}
	var lec  = {
		title : req.body.title || "-",
		day : req.body.day || "-",
		time : req.body.time || "-",
		period : req.body.period || "-",
		price : req.body.price || "-",
		num : req.params.num
	}

	
	db.pool.query("update program set title = :title, day = :day, time = :time, period=:period, price = :price where num=:num", 
		lec,
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json({result: "success"});
		}
	});
});
router.post("/program/lec", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}
	var lec  = {
		title : req.body.title || "-",
		day : req.body.day || "-",
		time : req.body.time || "-",
		period : req.body.period || "-",
		price : req.body.price || "-",
		branch: req.body.branch
	}

	
	db.pool.query("insert into program (title, day, time, period, price, branch) values (:title, :day, :time, :period, :price, :branch)", 
		lec,
		function(err, results){

		if(err){
			throw err;
		}else{
			var newNum = results.info.insertId;
			res.json({result: "success", num : newNum});
		}
	});
});
router.delete("/program/lec/:num", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}

	var num = req.params.num;

	
	db.pool.query("delete from program where num=:num", 
		{
			num:num
		},
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json({result: "success"});
		}
	});
});

router.put("/program/member/:num", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}
	var data = {
		title : req.body.title || "-",
		adult : req.body.adult || "-",
		student : req.body.student || "-",
		num : req.body.num
	}

	
	db.pool.query("update membership set title = :title, adult = :adult, student = :student where num=:num", 
		data,
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json({result: "success"});
		}
	});
});
router.post("/program/member", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}
	var data = {
		title : req.body.title || "-",
		adult : req.body.adult || "-",
		student : req.body.student || "-",
		branch : req.body.branch
	}

	
	db.pool.query("insert into membership (title, adult, student, branch) values (:title, :adult, :student, :branch)", 
		data,
		function(err, results){

		if(err){
			throw err;
		}else{
			var newNum = results.info.insertId;
			res.json({result: "success", num : newNum});
		}
	});
});
router.delete("/program/member/:num", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}

	var num = req.params.num;

	
	db.pool.query("delete from membership where num=:num", 
		{
			num:num
		},
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json({result: "success"});
		}
	});
});
router.put("/program/special/:num", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}
	var data = {
		title : req.body.title || "-",
		option : req.body.option || "-",
		price : req.body.price || "-",
		num : req.params.num
	}

	
	db.pool.query("update special_price set title = :title, option = :option, price= :price where num=:num", 
		data,
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json({result: "success"});
		}
	});
});
router.post("/program/special", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}
	var data = {
		title : req.body.title || "-",
		option: req.body.option|| "-",
		price: req.body.price|| "-",
		branch: req.body.branch
	}

	
	db.pool.query("insert into special_price (title, option, price, branch) values (:title, :option, :price, :branch)", 
		data,
		function(err, results){

		if(err){
			throw err;
		}else{
			var newNum = results.info.insertId;
			res.json({result: "success", num : newNum});
		}
	});
});
router.delete("/program/special/:num", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}

	var num = req.params.num;

	
	db.pool.query("delete from special_price where num=:num", 
		{
			num:num
		},
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json({result: "success"});
		}
	});
});

router.post("/insertNotice", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}

	var notice = {
		title : req.body.TITLE, 
		contents: req.body.CONTENTS,
		useyn : 'Y'
	}
	console.log(notice);

	db.pool.query("insert into notice (TITLE, CONTENTS, USE_YN, REGISTER_DATE) values (:title, :contents,:useyn, now())", 
		notice,
		function(err, results){

		if(err){
			throw err;
		}else{
			console.log(results);
			res.json({result: "success"});
		}
	});

	
});
router.put("/updateNotice/:id", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}

	var notice = {
		title : req.body.TITLE, 
		contents: req.body.CONTENTS,
		id : req.params.id,
	}
	console.log(notice);

	db.pool.query("update notice set TITLE = :title, CONTENTS = :contents where NOTICE_ID = :id", 
		notice,
		function(err, results){

		if(err){
			throw err;
		}else{
			console.log(results);
			res.json({result: "success"});
		}
	});

	
});

router.delete("/deleteNotice/:id", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}
	var id = req.params.id;
	
	//db.pool.query("update notice SET USE_YN = 'N' where NOTICE_ID=:id", 
	db.pool.query("delete from notice where NOTICE_ID = :id", 
		{
			id:id
		},
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json({result: "success"});
		}
	});
});

router.post('/naverUpload', noticeImg.single("file"), function(req, res, next) {
	

	var file = req.file;
	var ext = file.originalname.split('.').pop();
	var allowFile = {"jpg":true, "png":true, "bmp":true, "gif":true};
	if(!allowFile[ext]){
		res.json({result : "NOTALLOW_"+file.originalname });
	}else{

		var newPath = req.file.path+"."+ext ;

		fs.rename(file.path, newPath, function(err){
			if(err){
				throw err;
			}
			var ret = "";
			ret += "&bNewLine=true";
			ret += "&bNewLine=true";
			ret += "&sFileURL=/noticeImg/"+file.filename+"."+ext;
			res.json({result : ret});
		});
	}

});
router.put("/program/comment/:num", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}
	var data = {
		comment: req.body.comment || "",
		num : req.params.num
	}

	
	db.pool.query("update program_comment set comment = :comment where num=:num", 
		data,
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json({result: "success"});
		}
	});
});
router.post("/staff", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}
	var staff = {
		name : req.body.name,
		birth : req.body.birth,
		photo_path: req.body.photo_path,
		profile : req.body.profile,
		position : req.body.position
	}

	
	db.pool.query("insert into staff (name, birth, photo_path, profile, position) values (:name, :birth, :photo_path, :profile, :position)", 
		staff,
		function(err, results){

		if(err){
			throw err;
		}else{
			staff.num = results.info.insertId;
			res.json({result: "success", data: staff});
		}
	});
});
router.put("/staff/:num", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}
	var staff = {
		name : req.body.name,
		birth : req.body.birth,
		photo_path: req.body.photo_path,
		profile : req.body.profile,
		position : req.body.position,
		num : req.params.num
	}
	
	db.pool.query("update staff set name = :name, birth = :birth, photo_path = :photo_path, profile = :profile, position = :position where num = :num", 
		staff,
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json({result: "success", data: staff});
		}
	});
});
router.delete("/staff/:num", function(req, res, next){
	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}
	var num = req.params.num;
	
	db.pool.query("delete from staff where num = :num", 
		{num : num},
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json({result: "success"});
		}
	});
});

router.post('/staff/photo', profilePhoto.single("file"), function(req, res, next) {

	var sess = req.session;
	if(!sess.admin){
		res.send(401);
		return;
	}

	
	var httpRes = res;
	var ext = "";
	if(req.file.mimetype == "image/png"){
		ext = ".png";
	}else if(req.file.mimetype == "image/jpeg"){
		ext = ".jpg";
	}
	var newPath = req.file.path+ext ;


	fs.rename(req.file.path, newPath, function(err){
		if(err){
			throw err;
		}

		var filename = newPath.replace(/^.*[\\\/]/, '');
		easyimage.exec("convert "+newPath+" -print '%w %h' /dev/null")
		.then(function(res){

			var size = res.split(" ");
			var fileInfo = {
			};
			fileInfo.w = parseInt(size[0]);
			fileInfo.h = parseInt(size[1]);


			var dim = undefined;

			if(fileInfo.w > fileInfo.h){
				if(fileInfo.w > 120){
					fileInfo.h = Math.ceil((120/fileInfo.w) * fileInfo.h);
					fileInfo.w = 120;
					dim = fileInfo.w+"x"+fileInfo.h;
				}

			}else{
				if(fileInfo.h > 120){
					dim = "x120";
					fileInfo.w = Math.ceil((120/fileInfo.h) * fileInfo.w);
					fileInfo.h = 120;
					dim = fileInfo.w+"x"+fileInfo.h;

				}
			}
			if(dim){
				return easyimage.exec("convert "+newPath+" -resize "+dim+" "+newPath);
			}else{
				httpRes.json({photo_path : filename});
			}

		})
		.then(function(file){
			httpRes.json({photo_path : filename});
		})
		.catch(function(err){
			console.log(err);
		})


	});

});

module.exports = router;

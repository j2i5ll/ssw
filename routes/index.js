var express = require('express');
var router = express.Router();
var fs = require("fs");
var db = require("../modules/db");

var multer = require("multer");
var upload = multer({dest:__dirname+"/../public/album"});
var easyimage = require("easyimage");



/* GET home page. */
router.get('/', function(req, res, next) {
	var sess = req.session;
	var ua = req.header('user-agent');
	var sql="select NOTICE_ID, TITLE, CONTENTS, USE_YN, REGISTER_ID, REGISTER_DATE from notice order by notice_id desc limit 1";
	db.pool.query(sql, 
		function(err, results){
			var param = undefined;
			if(/mobile/i.test(ua)) {	//mobile
				if(err){
					param = undefined;
				}else{
					param = {noti : results.rows};
				}
				res.render("mobile", param);

			} else {
				if(err){
					param = undefined;
				}else{
					param = {
						noti:results.rows
					};
					if(sess.admin){
						param.user = true;
					}
					res.render("index", param);
				}
			}
		
	});
});
router.get('/gallery', function(req, res, next) {
	var count = req.query.count || 10;
	var idx = req.query.idx || 0;

	var limit = " limit ";
	count = parseInt(count);
	idx = parseInt(idx);
	if(isNaN(count) || isNaN(idx) || count < 0 || idx < 0){
		res.json({
			error : "NaN"
		});
	}else{
		limit += (idx+", "+count);

		var sql="select num, path, w, h, upload_date, description from gallery order by upload_date desc " + limit
		db.pool.query(sql, 
			function(err, results){

			if(err){
				throw err;
			}else{
				res.json(results.rows);
			}
		});
	}
});

router.get('/noticeList', function(req, res, next) {
	console.log('noticeList');
	var sql="select NOTICE_ID, TITLE, USE_YN, REGISTER_ID, REGISTER_DATE from notice where USE_YN = 'Y' order by REGISTER_DATE desc"
	db.pool.query(sql, 
		function(err, results){
			if(err){
				throw err;
			}else{
				res.json(results.rows);
		}
	});
	
});

router.get('/getNotice', function(req, res, next) {
	console.log('getNotice');
	var noticeId =  req.query.noticeId;
	
	var sql="select NOTICE_ID, TITLE, CONTENTS, USE_YN, REGISTER_ID, REGISTER_DATE from notice where NOTICE_ID = " + noticeId;
	db.pool.query(sql, 
		function(err, results){
			if(err){
				throw err;
			}else{
				res.json(results.rows);
		}
	});
});

router.get('/program/lec', function(req, res, next) {

	var branch = req.query.branch || 1;
	var sql="select num, title, day, time, period, price from program where branch=:branch order by branch asc, num asc";
	db.pool.query(sql, 
		{ branch : branch },
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json(results.rows);
		}
	});
});
router.get('/program/member', function(req, res, next) {

	var branch = req.query.branch || 1;
	var sql="select num, title, adult, student from membership where branch=:branch order by branch asc, num asc";
	db.pool.query(sql, 
		{ branch : branch },
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json(results.rows);
		}
	});
});

router.get('/program/special', function(req, res, next) {

	var branch = req.query.branch || 1;
	var sql="select num, title, option, price from special_price where branch=:branch order by branch asc, num asc";
	db.pool.query(sql, 
		{ branch : branch },
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json(results.rows);
		}
	});
});
router.get('/program/comments', function(req, res, next) {

	var branch = req.query.branch || 1;
	var sql="select num, prg_key, comment from program_comment where branch=:branch order by branch asc, num asc";
	db.pool.query(sql, 
		{ branch : branch },
		function(err, results){

		if(err){
			throw err;
		}else{
			var newRet= {};
			for(var i = 0;i<results.rows.length;i++){
				newRet[results.rows[i].prg_key] = results.rows[i];
			}
			res.json(newRet);
		}
	});
});
router.get('/staff', function(req, res, next) {
	var sql="select num, name, birth, photo_path, profile, position from staff";
	db.pool.query(sql, 
		function(err, results){

		if(err){
			throw err;
		}else{
			res.json(results.rows);
		}
	});
});
module.exports = router;

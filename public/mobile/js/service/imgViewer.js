sswApp.service('imgViewer', ["$compile", "$rootScope", "$controller", "$templateRequest", "$sce", function($compile, $rootScope, $controller, $templateRequest, $sce){
	var dialogScope ;
	var viewer;
	this.show = function(opt){
		var tmplUrl = $sce.getTrustedResourceUrl(opt.templateUrl);
		$templateRequest(tmplUrl).then(function(template){
			dialogScope = $rootScope.$new();
			var args = { $scope : dialogScope};
			if(opt.locals){
				for(var key in opt.locals){
					args[key] = opt.locals[key];
				}

			}
			$controller(opt.controller, args);
			viewer = $compile(template)(dialogScope);
			angular.element(document.body).append(viewer);
			angular.element(document.body)[0].classList.add("overlayed");
		}, function(){

		});
	}
	this.close = function(){
		dialogScope.$destroy();
		console.log(viewer[0]);
		document.body.removeChild(viewer[0]);
		angular.element(document.body)[0].classList.remove("overlayed");
	}
}]);

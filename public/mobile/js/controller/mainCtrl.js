sswApp.controller("mainCtrl", ["$scope", "$http", function($scope, $http){
	var ran = (Math.floor((Math.random() * 100) % 8) + 1)
	$scope.background = {
		'background-image':'url(/mobile/img/main/'+ran +'.jpg)'
	}

	$scope.branchPanImg = [];
	$scope.branchPanImg.push({ path: '/photo/pan/1.jpg'})
	$scope.branchPanImg.push({ path: '/photo/pan/2.jpg'})
	$scope.branchPanImg.push({ path: '/photo/pan/3.jpg'})
	$scope.branchPanImg.push({ path: '/photo/pan/4.jpg'})
	$scope.branchPanImg.push({ path: '/photo/pan/5.jpg'})
	$scope.branchPanImg.push({ path: '/photo/pan/6.jpg'})



	$scope.branchJamImg = [];
	$scope.branchJamImg.push({ path: '/photo/jam/1.jpg', dim: 'x504', col:2, row:2 })
	$scope.branchJamImg.push({ path: '/photo/jam/2.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchJamImg.push({ path: '/photo/jam/3.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchJamImg.push({ path: '/photo/jam/4.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchJamImg.push({ path: '/photo/jam/5.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchJamImg.push({ path: '/photo/jam/6.jpg', dim: 'x504', col:2, row:2 })
	$scope.branchJamImg.push({ path: '/photo/jam/7.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchJamImg.push({ path: '/photo/jam/8.jpg', dim: '504x', col:2, row:1 })

	$scope.getGalImg = function(){
		
		var galGrid = [
			{col:2, row:2},
			{col:1, row:1},
			{col:1, row:1},
			{col:2, row:2},
			{col:1, row:1},
			{col:2, row:2},
			{col:1, row:1},
			{col:1, row:1},
			{col:1, row:1},
			{col:1, row:1},
			{col:1, row:1},
		];

		$scope.galleryTile = [];

		$http({
			method : "GET",
			url : "/gallery?count=6&idx=0"
		}).then(function(response){
			var galList = response.data;
			$scope.galleryTile = response.data;

		}, function(response){
		});

	}
	$scope.topNoti = topNoti[0];

	$scope.getGalImg();
}]);


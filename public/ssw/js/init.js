sswApp.config(["$stateProvider", "$urlRouterProvider", "userProvider", "$httpProvider", function($stateProvider, $urlRouterProvider, userProvider, $httpProvider){


        $stateProvider
        .state("son", {
                url:"/son",
                views:{
                        "content":{
                                templateUrl:"/ssw/tmpl/son.html",
				controller:"mainCtrl"
                        },
			"header":{
                                templateUrl:"/ssw/tmpl/header.html",
				controller:"headerCtrl"
			}

                }
        })
        .state("/", {
                url:"/",
                views:{
                        "content":{
                                templateUrl:"/ssw/tmpl/main.html",
				controller:"mainCtrl"
                        },
			"header":{
                                templateUrl:"/ssw/tmpl/header.html",
				controller:"headerCtrl"
			}

                }
        })
        .state("noticeList", {
                url:"/noticeList",
                views:{
                        "content":{
							templateUrl:"/ssw/tmpl/noticeList.html",
							controller:"noticeListCtrl"
                        },
						"header":{
							templateUrl:"/ssw/tmpl/header.html",
							controller:"headerCtrl"
						}
				}
        })
		.state("noticeRegist", {
                url:"/noticeRegist/:id",
                views:{
                        "content":{
							templateUrl:"/ssw/tmpl/noticeRegist.html",
							controller:"noticeRegistCtrl"
					     },
						"header":{
							templateUrl:"/ssw/tmpl/header.html",
							controller:"headerCtrl"
						}
                }
        })
		.state("notice", {
                url:"/notice/:id",
                views:{
                        "content":{
							templateUrl:"/ssw/tmpl/notice.html",
							controller:"noticeCtrl"
                        },
						"header":{
							templateUrl:"/ssw/tmpl/header.html",
							controller:"headerCtrl"
						}
                }
        })
        .state("gallery", {
                url:"/gallery",
                views:{
                        "content":{
                                templateUrl:"/ssw/tmpl/gallery.html",
				controller:"galleryCtrl"
                        },
			"header":{
				
				templateUrl:"/ssw/tmpl/header.html",
				controller:"headerCtrl"
			}
                }
        })
        .state("location", {
                url:"/location",
                views:{
                        "content":{
                                templateUrl:"/ssw/tmpl/location.html",
				controller:"locationCtrl"
                        },
			"header":{
				
				templateUrl:"/ssw/tmpl/header.html",
				controller:"headerCtrl"
			}
                }
        })
        .state("staff", {
                url:"/staff",
                views:{
                        "content":{
                                templateUrl:"/ssw/tmpl/staff.html",
				controller:"staffCtrl"
                        },
			"header":{
				
				templateUrl:"/ssw/tmpl/header.html",
				controller:"headerCtrl"
			}
                }
        })
        .state("program", {
                url:"/program",
                views:{
                        "content":{
                                templateUrl:"/ssw/tmpl/program.html",
				controller:"programCtrl"
                        },
			"header":{
				templateUrl:"/ssw/tmpl/header.html",
				controller:"headerCtrl"
			}

                }
        });
	if(userProvider.getUser()){
		$stateProvider
		.state("program_write", {
			url:"/program/write",
			views:{
				"content":{
					templateUrl:"/ssw/tmpl/program_write.html",
					controller:"programWriteCtrl"
				},
				"header":{
					templateUrl:"/ssw/tmpl/header.html",
					controller:"headerCtrl"
				}

			}
		})
		.state("staff_write", {
			url:"/staff/write",
			views:{
				"content":{
					templateUrl:"/ssw/tmpl/staff_write.html",
					controller:"staffWriteCtrl"
				},
				"header":{
					templateUrl:"/ssw/tmpl/header.html",
					controller:"headerCtrl"
				}

			}
		})
	}
	/*
        .state("program", {
                url:"/program",
                views:{
                        "content":{
                                templateUrl:"/tmpl/program.html",
                        }

                }
        })
        .state("address", {
                url:"/address",
                views:{
                        "content":{
                                templateUrl:"/tmpl/address.html",
                                controller:"addressCtrl"
                        }

                }
        });
	*/
        $urlRouterProvider.otherwise("/");
}]);
sswApp.provider("user", [function(){
	
	var user = window.user;

	return {
		$get : function(){
			return {
				getUser : function(){
					return user;
				}
			}
		},
		getUser : function(){
			return user;
		}
	}
}]);

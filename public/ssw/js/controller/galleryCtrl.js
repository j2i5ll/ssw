sswApp.controller("galleryCtrl", ["$scope", "$mdDialog", "$http", "imgViewer", "user", function($scope, $mdDialog, $http, imgViewer, user){
	$scope.user = user.getUser();
	$scope.uploadImg= function(ev){
		$mdDialog.show({
			controller:"imageUploadCtrl",
			templateUrl : "/ssw/tmpl/imageUpload.html",
			targetEvent:ev,
			parent: angular.element(document.body)
		}).then(function(list){
		}, function(){
		});
	};

	$scope.showImage = function(idx){
		var opt = {
			idx : idx,
			list : $scope.galleryList
		};
		imgViewer.show({
			controller:"galleryImgViewerCtrl",
			templateUrl:"/ssw/tmpl/imgViewer.html",
			locals:{
				listObj : opt
			}
		});
	};

	$scope.galleryList = [];
	
	var count = 24;
	var isEnd = false;
	$scope.moreImage = function(){
		if(isEnd){
			return;
		}
		$scope.galleryBusy = true;
		$http({
			method : "GET",
			url : "/gallery?count="+count+"&idx="+$scope.galleryList.length
		}).then(function(response){
			if(count == 24){
				count = 12;
			}
			var addList = response.data;
			if(addList.length == 0){
				isEnd = true;
			}
			for(var i =0;i<addList.length;i++){
				addList[i].path = "/photo/"+addList[i].path;
				$scope.galleryList.push(addList[i]);
			}
			$scope.galleryBusy = false;
		}, function(response){
		});

	};


	$scope.del= function(idx, ev){
		var confirm = $mdDialog.confirm()
			.title('사진을 삭제하시겠습니까?')
			.ariaLabel('Delete photo')
			.targetEvent(ev)
			.ok('삭제')
			.cancel('취소');
		$mdDialog.show(confirm).then(function() {
			$http({
				method : "DELETE",
				url : "/admin/gallery/"+$scope.galleryList[idx].num
			}).then(function(res){
				console.log(res);
				if(res.data.result == "success"){
					$scope.galleryList.splice(idx, 1);
				}
			}, function(err){
				console.log(err);
			});
		}, function() {
		});


	};
}]);
sswApp.controller("galleryImgViewerCtrl", ["$scope", "imgViewer", "listObj", "$http", function($scope, imgViewer, listObj, $http){
	$scope.viewer = listObj;

	$scope.getImageUrl = function(){
		$scope.selImg = $scope.viewer.list[$scope.viewer.idx];
		console.log($scope.selImg);
		console.log($scope.selImg);
	};
	$scope.close = function(){
		imgViewer.close();
	};
	$scope.next = function(){
		$scope.viewer.idx ++;
		$scope.getImageUrl();
		if($scope.viewer.idx == $scope.viewer.list.length - 1){
			$http({
				method : "GET",
				url : "/gallery?count=10&idx="+($scope.viewer.idx+1)
			}).then(function(response){
				console.log(response);
				var addList = response.data;
				for(var i =0;i<addList.length;i++){
					addList[i].path = "/photo/"+addList[i].path;
					$scope.viewer.list.push(addList[i]);
				}

			}, function(response){
			});

		}
	};
	$scope.prev= function(){
		$scope.viewer.idx --;
		$scope.getImageUrl();
	};
	$scope.getImageUrl();

		
}]);

sswApp.controller("imageUploadCtrl", ["$scope", "$mdDialog", "FileUploader", function($scope, $mdDialog, FileUploader){
	$scope.close = function(){
		$mdDialog.cancel();
	}
	$scope.photo = {
		desc :""
	}


	FileUploader.FileSelect.prototype.isEmptyAfterSelection = function(){
                return false;
        }

        $scope.uploader = new FileUploader({
                url:"/admin/gallery/upload",
		queueLimit : 5 
        });
        $scope.uploader.filters.push({
                name:"extFilter",
                fn : function(item){
			var  name = item.name.toLowerCase();
			var ext = name.split(".").pop();
                        if(ext == "png" || ext == "jpg"){
                                return true;
                        }else{
                                return false;
                        }
                }
        },{
		name : "sizeFilter",
		fn : function(item){
			if(item.size > 10*1024*1024 ){
				return false;
			}else{
				return true;
			}
		}
	}
	);
	var totalProgress = 0;

	$scope.uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        };
        $scope.uploader.onAfterAddingFile = function(fileItem) {

        };
        $scope.uploader.onAfterAddingAll = function(addedFileItems) {
		document.querySelector("input[type=file]").value="";
        };
        $scope.uploader.onBeforeUploadItem = function(item) {
		console.log("onBeforeUploadItem");
		item.formData.push({
			desc : $scope.photo.desc
		});
		item.inProgress= true;
		console.log(item)
        };
        $scope.uploader.onProgressItem = function(fileItem, progress) {
        };
        $scope.uploader.onProgressAll = function(progress) {
        };
        $scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
		fileItem.inProgress= false;
		/*
            if(!response.error){

            }else{
                    console.log(response.result);
                    if(response.result.status == "success"){
                            $mdDialog.hide(response.result.list);
                    }else{
                            $mdDialog.hide("error");
                    }


            }
	    */
        };
        $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
        };
        $scope.uploader.onCancelItem = function(fileItem, response, status, headers) {
        };
        $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
        };
        $scope.uploader.onCompleteAll = function() {
        };



}]);

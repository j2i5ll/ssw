sswApp.controller("staffCtrl", ["$scope", "$http", function($scope, $http){

	$scope.staffList = [];
	$http({
		url :"/staff",
		method:"GET"
	}).success(function(data){
		$scope.staffList = data;
	});

}]);

sswApp.controller("mainCtrl", ["$scope", "imgViewer", "$http", function($scope, imgViewer, $http){
	$scope.branchPanImg = [];
	$scope.branchPanImg.push({ path: '/photo/pan/1.jpg', dim: 'x504', col:3, row:2 })
	$scope.branchPanImg.push({ path: '/photo/pan/2.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchPanImg.push({ path: '/photo/pan/3.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchPanImg.push({ path: '/photo/pan/4.jpg', dim: 'x504', col:2, row:2 })
	$scope.branchPanImg.push({ path: '/photo/pan/5.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchPanImg.push({ path: '/photo/pan/6.jpg', dim: '504x', col:2, row:1 })



	$scope.branchJamImg = [];
	$scope.branchJamImg.push({ path: '/photo/jam/1.jpg', dim: 'x504', col:2, row:2 })
	$scope.branchJamImg.push({ path: '/photo/jam/2.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchJamImg.push({ path: '/photo/jam/3.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchJamImg.push({ path: '/photo/jam/4.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchJamImg.push({ path: '/photo/jam/5.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchJamImg.push({ path: '/photo/jam/6.jpg', dim: 'x504', col:2, row:2 })
	$scope.branchJamImg.push({ path: '/photo/jam/7.jpg', dim: 'x250', col:1, row:1 })
	$scope.branchJamImg.push({ path: '/photo/jam/8.jpg', dim: '504x', col:2, row:1 })

	$scope.mainImg = [
		'/ssw/img/main/1.jpg',
		'/ssw/img/main/2.jpg',
		'/ssw/img/main/3.jpg',
		'/ssw/img/main/4.jpg',
		'/ssw/img/main/5.jpg',
		'/ssw/img/main/6.jpg'
	];
	var ran = Math.floor((Math.random() * 100) % 6)
	$scope.mainBackgroundStyle = {
		'background-image': 'url('+$scope.mainImg[ran ]+')'
	}



	$scope.tileImgs = [
		{
			path : "/photo/tile/t1.jpg",
		},
		{
			path : "/photo/tile/t2.jpg",
		},
		{
			path : "/photo/tile/t3.jpg",
		},
		{
			path : "/photo/tile/t4.jpg",
		},
		{
			path : "/photo/tile/t5.jpg",
		},
		{
			path : "/photo/tile/t6.jpg",
		},
		{
			path : "/photo/tile/t7.jpg",
		},
		{
			path : "/photo/tile/t8.jpg"
		}
	];
	$scope.showBigSize = function(idx){
		var opt= {
			idx:idx,
			list : $scope.tileImgs,
		}
		imgViewer.show({
			controller:"mainImgViewerCtrl", 
			templateUrl :"/ssw/tmpl/imgViewer.html" ,
			locals:{
				listObj : opt	
			}
		});
	}

	$scope.getGalImg = function(){
		
		var galGrid = [
			{col:2, row:2},
			{col:1, row:1},
			{col:1, row:1},
			{col:2, row:2},
			{col:1, row:1},
			{col:2, row:2},
			{col:1, row:1},
			{col:1, row:1},
			{col:1, row:1},
			{col:1, row:1},
			{col:1, row:1},
		];

		$scope.galleryTile = [];

		$http({
			method : "GET",
			url : "/gallery?count=11&idx=0"
		}).then(function(response){
			console.log(response);
			var galList = response.data;
			for(var i =0;i<galList.length;i++){
				var url = "/photo/"+galList[i].path;
				var ngClass=undefined; 
				if(galGrid[i].col == 1){
					if(galList[i].h > 300){
						//url+="?dim=x300";
						ngClass = "full";

					}
				}else if(galGrid[i].col == 2){
					if(galList[i].h > 600){
						//url+="?dim=x600";
						ngClass = "full";
					}
				}
				$scope.galleryTile.push({
					span : galGrid[i],
					path : url,
					upload_date : galList[i].upload_date,
					w : galList[i].w,
					h : galList[i].h,
					full : ngClass,
				});
			}


		}, function(response){
		});

	}

	$scope.showBranchImage = function(idx, list){
		var opt= {
			idx:idx,
			list : list
		}
		imgViewer.show({
			controller:"mainImgViewerCtrl", 
			templateUrl :"/ssw/tmpl/imgViewer.html" ,
			locals:{
				listObj : opt	
			}
		});
	}

	$scope.showGallery = function(idx){
		var opt= {
			idx:idx,
			list : $scope.galleryTile,
		}
		imgViewer.show({
			controller:"mainImgViewerCtrl", 
			templateUrl :"/ssw/tmpl/imgViewer.html" ,
			locals:{
				listObj : opt	
			}
		});
	}

	$scope.getGalImg();

}]);

sswApp.controller("mainImgViewerCtrl", ["$scope", "imgViewer", "listObj", "$http", function($scope, imgViewer, listObj, $http){
        $scope.viewer = listObj;

	$scope.getImageUrl = function(){
		$scope.selImg = $scope.viewer.list[$scope.viewer.idx];
		$scope.nowImgSrc = $scope.viewer.list[$scope.viewer.idx].path;
		//$scope.nowImgSrc = $scope.nowImgSrc.split("?")[0]+"?dim=x900";
	}

        $scope.close = function(){
                imgViewer.close();
        }
        $scope.next = function(){
                $scope.viewer.idx ++;
		$scope.getImageUrl();
        }
        $scope.prev = function(){
                $scope.viewer.idx --;
		$scope.getImageUrl();
        }
	$scope.getImageUrl(); 

}]);


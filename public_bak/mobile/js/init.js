sswApp.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider){

        $stateProvider
        .state("/", {
                url:"/",
                views:{
                        "content":{
                                templateUrl:"/mobile/tmpl/main.html",
				controller:"mainCtrl"
                        },
			"header":{
                                templateUrl:"/mobile/tmpl/header.html",
				controller:"headerCtrl"
			}

                }
        })
	.state("notice", {
                url:"/notice/:id",
                views:{
                        "content":{
				templateUrl:"/mobile/tmpl/notice.html",
				controller:"noticeCtrl"
                        },
			"header":{
				templateUrl:"/mobile/tmpl/header.html",
				controller:"headerCtrl"
			}
                }
        })
        .state("noticeList", {
                url:"/noticeList",
                views:{
                        "content":{
				templateUrl:"/mobile/tmpl/noticeList.html",
				controller:"noticeListCtrl"
                        },
			"header":{
				templateUrl:"/mobile/tmpl/header.html",
				controller:"headerCtrl"
			}
		}
        })
        .state("gallery", {
                url:"/gallery",
                views:{
                        "content":{
                                templateUrl:"/mobile/tmpl/gallery.html",
				controller:"galleryCtrl"
                        },
			"header":{
				
				templateUrl:"/mobile/tmpl/header.html",
				controller:"headerCtrl"
			}
                }
        })
        .state("staff", {
                url:"/staff",
                views:{
                        "content":{
                                templateUrl:"/mobile/tmpl/staff.html",
				controller:"staffCtrl"
                        },
			"header":{
				
				templateUrl:"/mobile/tmpl/header.html",
				controller:"headerCtrl"
			}
                }
        })
        .state("location", {
                url:"/location",
                views:{
                        "content":{
                                templateUrl:"/mobile/tmpl/location.html",
				controller:"locationCtrl"
                        },
			"header":{
				
				templateUrl:"/mobile/tmpl/header.html",
				controller:"headerCtrl"
			}
                }
        })
        .state("program", {
                url:"/program",
                views:{
                        "content":{
                                templateUrl:"/mobile/tmpl/program.html",
				controller:"programCtrl"
                        },
			"header":{
				templateUrl:"/mobile/tmpl/header.html",
				controller:"headerCtrl"
			}

                }
        });
	/*
        .state("program", {
                url:"/program",
                views:{
                        "content":{
                                templateUrl:"/tmpl/program.html",
                        }

                }
        })
        .state("address", {
                url:"/address",
                views:{
                        "content":{
                                templateUrl:"/tmpl/address.html",
                                controller:"addressCtrl"
                        }

                }
        });
	*/
        $urlRouterProvider.otherwise("/");
}]);


sswApp.controller("addressCtrl", ["$scope", function($scope){
	var lat = 37.402120, lng= 127.107019;
	var markers = [
		{
			position: new daum.maps.LatLng(lat, lng), 
			text: '손상원클라이밍짐' 
		}
	];

	var staticMapContainer  = document.getElementById('map'), 
	staticMapOption = { 
	center: new daum.maps.LatLng(lat, lng),
	level: 3, 
	marker: markers 
	};    
	var staticMap = new daum.maps.StaticMap(staticMapContainer, staticMapOption);

}]);

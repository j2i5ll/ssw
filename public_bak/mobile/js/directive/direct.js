sswApp.directive("fullSection", ["$window", function($window){
	return {
		restrict:"A",
		scope:{},
		link : function(scope, elem, attr){

			elem[0].style.width= "100%";

			if(attr.fullSection){
				var heightVal = attr.fullSection;
				var num = /[0-9]+/.exec(heightVal)[0];
				var unit = /[0-9]+([^ ,\)`]*)/.exec(heightVal)[1];
				if(unit == "%"){
					elem[0].style.minHeight= Math.ceil(($window.innerHeight * (num / 100))) +"px";
				}else{
					elem[0].style.minHeight= attr.fullSection;
				}
			}else{
				elem[0].style.minHeight= "800px";
				scope.initializeWindowSize = function () {
					elem[0].style.minHeight= $window.innerHeight+"px";
				};
				scope.initializeWindowSize();
				angular.element($window).bind('resize', function () {
					scope.initializeWindowSize();
				});
			}
		}
	}
	
}]);
sswApp.directive("mainHeader", ["$window", function($window){
	return {
		restrict:"A",
		scope:{},
		link : function(scope, elem, attr){
			angular.element($window).bind("scroll", function(){
				var pageYOffset = $window.pageYOffset;	
				if(pageYOffset > 0){
					elem[0].classList.add("sticky");
					elem[0].classList.add("md-whiteframe-4dp");
				}else{
					elem[0].classList.remove("sticky");
					elem[0].classList.remove("md-whiteframe-4dp");
				}
			});

			
		}
	}
	
}]);

sswApp.directive("logo", [function(){

	return {
		restrict:"E",
		scope:{
		},
		template:"<div class='text-logo' ><span class='son' >SON</span> <span class='sw' >SANG WON</span><br/><span class='cg' >CLIMBING GYM</span></div>"
	}

}]);

sswApp.controller("mainCtrl", ["$scope", "$http", function($scope, $http){
	$scope.tileImgs = [
		{
			path : "/photo/tile/t1.jpg",
		},
		{
			path : "/photo/tile/t2.jpg",
		},
		{
			path : "/photo/tile/t3.jpg",
		},
		{
			path : "/photo/tile/t4.jpg",
		},
		{
			path : "/photo/tile/t5.jpg",
		},
		{
			path : "/photo/tile/t6.jpg",
		},
		{
			path : "/photo/tile/t7.jpg",
		},
		{
			path : "/photo/tile/t8.jpg"
		}
	];

	$scope.getGalImg = function(){
		
		var galGrid = [
			{col:2, row:2},
			{col:1, row:1},
			{col:1, row:1},
			{col:2, row:2},
			{col:1, row:1},
			{col:2, row:2},
			{col:1, row:1},
			{col:1, row:1},
			{col:1, row:1},
			{col:1, row:1},
			{col:1, row:1},
		];

		$scope.galleryTile = [];

		$http({
			method : "GET",
			url : "/gallery?count=6&idx=0"
		}).then(function(response){
			var galList = response.data;
			$scope.galleryTile = response.data;

		}, function(response){
		});

	}
	$scope.topNoti = topNoti[0];

	$scope.getGalImg();
}]);


sswApp.controller("noticeListCtrl", ["$scope", "$http", "$anchorScroll", function($scope, $http, $anchorScroll){
	$scope.pageSize = 10;
	$scope.pageLength = 0;
	$scope.noticeList = undefined;
	$scope.currentPageSet = 0;
	$scope.currentPageItems =[];
	
	$scope.initialize = function() {
		console.log('notice init');
		var searchInfo ={};
		$http.get("/noticeList", searchInfo)
		.success(function(data) {
			$scope.noticeList = data;
			$scope.pageLength = $scope.noticeList.length / $scope.pageSize + 1;
			$scope.currentPageItems = $scope.noticeList.slice(0, $scope.pageSize);
		})
		.error(function(data) {
			alert('리스트를 받아오지 못했습니다.');
		});

	};
	$scope.initialize();
	
	$scope.selectPage = function(pageNum){
		$anchorScroll.yOffset=0;
		$anchorScroll();
		$scope.currentPageItems = $scope.noticeList.slice((pageNum-1)*$scope.pageSize, pageNum*$scope.pageSize);      

    };
	$scope.prevPageSet = function(){
		if($scope.currentPageSet < 10)
			return;
		$scope.currentPageSet -= 10;
		$scope.selectPage($scope.currentPageSet+10);
    };
	$scope.nextPageSet = function(){
		if($scope.currentPageSet+10 >  $scope.pageLength)
			return;
		$scope.currentPageSet += 10;
		$scope.selectPage($scope.currentPageSet+1);
    };

	$scope.pageLimit = function() {
		if(($scope.pageLength-$scope.currentPageSet)>10)
			return 10;
		else
			return $scope.pageLength-$scope.currentPageSet;
	};

	var now = new Date();
	$scope.isNew = function(notice){
		var notiDate = new Date(
				notice.REGISTER_DATE.substring(0, 4),
				notice.REGISTER_DATE.substring(5, 7)*1-1,
				notice.REGISTER_DATE.substring(8, 10),
				notice.REGISTER_DATE.substring(11, 13),
				notice.REGISTER_DATE.substring(14, 16),
				notice.REGISTER_DATE.substring(17, 19)
				);
		var timeDiff = Math.abs(now.getTime() - notiDate.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
		if(diffDays > 6){
			return false;
		}else{
			return true;
		}

	}

}])
.controller("noticeCtrl", ["$scope", "$http", "$stateParams", function($scope, $http, $stateParams){
	$scope.noticeId = $stateParams.id;
  
	$scope.initialize = function() {
		console.log('noticeCtrl');
		$http({
			method:"GET",
			url:"/getNotice?noticeId="+$scope.noticeId		
		}).then(function(data, status){
			$scope.currentNotice = data.data;
		}, function(err, status){
			console.log(err);
			alert('공지사항을 불러오지못했습니다.');
		});
	};
	$scope.initialize();

}])

.filter('pageCount', function() {
        return function(data, size) {
            if(angular.isArray(data)) {
                var result = [];
                for (var i = 0; i < Math.ceil(data.length / size); i++) {
                    result.push(i);
                }
                return result;
            } else {
                return data;
            }
        };
});

sswApp.factory("programData", [function(){

	var data = { 
		data : {},
		alreadyGet : false
	};

	return data;
}]);
sswApp.controller("programCtrl", ["$scope", "programData", "$http", "$q", function($scope, programData, $http, $q){

	$scope.tab = 1
	$scope.showTab = function (tabIdx) {
		$scope.tab = tabIdx;
		$scope.req($scope.tab)
	}

	$scope.req = function (idx) {

		var lectureQ = $http({
			method:"GET",
			url : "/program/lec?branch="+idx
		});
		var memberQ = $http({
			method:"GET",
			url : "/program/member?branch="+idx
		});
		var specialQ = $http({
			method:"GET",
			url : "/program/special?branch="+idx
		});
		var commentQ = $http({
			method:"GET",
			url : "/program/comments?branch="+idx

		});

		$q.all([lectureQ, memberQ, specialQ, commentQ])
		.then(function(data){
			programData.alreadyGet = true;

			for(var i = 0;i<data.length;i++){
				if(data[i].config.url.split('?')[0] == "/program/lec"){
					programData.data.lecture={};
					programData.data.lecture.list = data[i].data;
					programData.data.lecture.title = [
						"구분",
						"요일",
						"시간",
						"기간",
						"강습료"
					];
					programData.data.lecture.flex = [
						20,15,25, 20, 20
					];

				}else if(data[i].config.url.split('?')[0] == "/program/member"){
					programData.data.member={};
					programData.data.member.list = data[i].data;
					programData.data.member.title=[
						"구분",
						"성인",
						"학생"
					];
				}else if(data[i].config.url.split('?')[0] == "/program/special"){

					programData.data.special = {};
					programData.data.special.list = data[i].data;
					programData.data.special.title = [
						"구분",
						"옵션",
						"가격"
					];
				}else if(data[i].config.url.split('?')[0] == "/program/comments"){
					programData.data.comment = data[i].data;
				}

			}
			$scope.prgItems = programData.data;

		});

	}
	$scope.showTab(1)

}]);

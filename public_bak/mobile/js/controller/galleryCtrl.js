sswApp.controller("galleryCtrl", ["$scope", "$mdDialog", "$http", "imgViewer", function($scope, $mdDialog, $http, imgViewer){

	$scope.showImage = function(idx){
		var opt = {
			idx : idx,
			list : $scope.galleryList
		};
		imgViewer.show({
			controller:"galleryImgViewerCtrl",
			templateUrl:"/mobile/tmpl/imgViewer.html",
			locals:{
				listObj : opt
			}
		});
	};

	$scope.galleryList = [];
	
	var count = 24;
	var isEnd = false;
	$scope.moreImage = function(){
		if(isEnd){
			return;
		}
		$scope.galleryBusy = true;
		$http({
			method : "GET",
			url : "/gallery?count="+count+"&idx="+$scope.galleryList.length
		}).then(function(response){
			if(count == 24){
				count = 12;
			}
			var addList = response.data;
			if(addList.length == 0){
				isEnd = true;
			}
			for(var i =0;i<addList.length;i++){
				addList[i].path = "/photo/"+addList[i].path;
				$scope.galleryList.push(addList[i]);
			}
			$scope.galleryBusy = false;
		}, function(response){
		});

	};

}]);
sswApp.controller("galleryImgViewerCtrl", ["$scope", "imgViewer", "listObj", "$http", function($scope, imgViewer, listObj, $http){
	$scope.viewer = listObj;

	$scope.getImageUrl = function(){
		$scope.selImg = $scope.viewer.list[$scope.viewer.idx];
	};
	$scope.close = function(){
		imgViewer.close();
	};
	$scope.next = function(){
		$scope.viewer.idx ++;
		$scope.getImageUrl();
		if($scope.viewer.idx == $scope.viewer.list.length - 1){
			$http({
				method : "GET",
				url : "/gallery?count=10&idx="+($scope.viewer.idx+1)
			}).then(function(response){
				console.log(response);
				var addList = response.data;
				for(var i =0;i<addList.length;i++){
					addList[i].path = "/photo/"+addList[i].path;
					$scope.viewer.list.push(addList[i]);
				}

			}, function(response){
			});

		}
	};
	$scope.prev= function(){
		$scope.viewer.idx --;
		$scope.getImageUrl();
	};
	$scope.getImageUrl();

		
}]);


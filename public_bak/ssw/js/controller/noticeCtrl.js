sswApp.controller("noticeListCtrl", ["$scope", "$http", "$anchorScroll", "user", function($scope, $http, $anchorScroll, user){
	$scope.user = user.getUser();

	$scope.pageSize = 10;
	$scope.pageLength = 0;
	$scope.noticeList = undefined;
	$scope.currentPageSet = 0;
	$scope.currentPageItems =[];
	
	$scope.initialize = function() {
		console.log('notice init');
		var searchInfo ={};
		$http.get("/noticeList", searchInfo)
		.success(function(data) {
			$scope.noticeList = data;
			$scope.pageLength = $scope.noticeList.length / $scope.pageSize + 1;
			$scope.currentPageItems = $scope.noticeList.slice(0, $scope.pageSize);
		})
		.error(function(data) {
			alert('리스트를 받아오지 못했습니다.');
		});

	};
	$scope.initialize();
	
	$scope.selectPage = function(pageNum){
		$anchorScroll.yOffset=0;
		$anchorScroll();
		$scope.currentPageItems = $scope.noticeList.slice((pageNum-1)*$scope.pageSize, pageNum*$scope.pageSize);      

    };
	$scope.prevPageSet = function(){
		if($scope.currentPageSet < 10)
			return;
		$scope.currentPageSet -= 10;
		$scope.selectPage($scope.currentPageSet+10);
    };
	$scope.nextPageSet = function(){
		if($scope.currentPageSet+10 >  $scope.pageLength)
			return;
		$scope.currentPageSet += 10;
		$scope.selectPage($scope.currentPageSet+1);
    };

	$scope.pageLimit = function() {
		if(($scope.pageLength-$scope.currentPageSet)>10)
			return 10;
		else
			return $scope.pageLength-$scope.currentPageSet;
	};

	var now = new Date();
	$scope.isNew = function(notice){
		var notiDate = new Date(
				notice.REGISTER_DATE.substring(0, 4),
				notice.REGISTER_DATE.substring(5, 7)*1-1,
				notice.REGISTER_DATE.substring(8, 10),
				notice.REGISTER_DATE.substring(11, 13),
				notice.REGISTER_DATE.substring(14, 16),
				notice.REGISTER_DATE.substring(17, 19)
				);
		var timeDiff = Math.abs(now.getTime() - notiDate.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
		if(diffDays > 6){
			return false;
		}else{
			return true;
		}

	}

}])
.controller("noticeRegistCtrl", ["$scope", "$http","$stateParams", "$state", "$mdDialog", function($scope, $http,$stateParams, $state, $mdDialog){
	console.log('noticeRegistCtrl');

	var mode = "write";
	if($stateParams.id !=0){
		mode = "modify";

	}
	$scope.init = function(){
		if(mode == "modify"){
			$scope.noticeId = $stateParams.id;
			$http({
				method:"GET",
				url:"/getNotice?noticeId="+$scope.noticeId		
			}).then(function(data, status){
				$scope.notice= data.data[0];
			}, function(err, status){
				console.log(err);
				alert('공지사항을 불러오지못했습니다.');
			});
		}
	}

	$scope.saveNotice = function(){
		if(!$scope.notice || !$scope.notice.TITLE){
			$mdDialog.show(
				$mdDialog.alert()
				.parent(angular.element(document.body))
				.title('공지 작성')
				.textContent('제목을 입력하세요.')
				.ariaLabel('Title')
				.ok('확인')
			);

			return;
		}
		var url ="";
		var method ="";
		if(mode == "write"){
			method="POST";
			url = "/admin/insertNotice";
		}else if(mode == "modify"){
			method="PUT";
			url = "/admin/updateNotice/"+$scope.notice.NOTICE_ID;
		}
		var inputContents = oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
		$scope.notice.CONTENTS= $('#ir1').val();
		$http({
			method : method,
			url : url,
			data : $scope.notice
		}).then(function(data, status){
			if(mode == "write"){
				$state.go("noticeList");
			}else if(mode == "modify"){
				$state.go("notice", {id:$scope.notice.NOTICE_ID});
			}
			
		}, function(err, status){
			console.log(err);
			alert('저장하지못했습니다.');
		});
		
	};
	$scope.init ();

}])
.controller("noticeCtrl", ["$scope", "$http", "$stateParams", "$mdDialog", "$state", "user", function($scope, $http, $stateParams, $mdDialog, $state, user){
	$scope.user = user.getUser();
	$scope.noticeId = $stateParams.id;
  
	$scope.initialize = function() {
		console.log('noticeCtrl');
		$http({
			method:"GET",
			url:"/getNotice?noticeId="+$scope.noticeId		
		}).then(function(data, status){
			$scope.currentNotice = data.data;
		}, function(err, status){
			console.log(err);
			alert('공지사항을 불러오지못했습니다.');
		});
	};
	$scope.initialize();

	$scope.deleteNotice = function(){
		var c = $mdDialog.confirm()
			.title('삭제하시겠습니까?')
			.textContent('선택한 공지사항을 삭제하시겠습니까?')
			.ariaLabel('del notice')
			.targetEvent()
			.ok('삭제')
			.cancel('취소');
		$mdDialog.show(c).then(function() {
			$http({
				method:"DELETE",
				url :"/admin/deleteNotice/"+$scope.currentNotice[0].NOTICE_ID
			}).then(function(data, status){
				if(data.data.result == "success"){
					$state.go("noticeList");
				}
			}, function(err, status){
			});

		}, function() {
		});
	};

	$scope.modifyNotice = function(){
		$state.go("noticeRegist", {id : $scope.currentNotice[0].NOTICE_ID});
	};

}])
.filter('pageCount', function() {
        return function(data, size) {
            if(angular.isArray(data)) {
                var result = [];
                for (var i = 0; i < Math.ceil(data.length / size); i++) {
                    result.push(i);
                }
                return result;
            } else {
                return data;
            }
        };
});

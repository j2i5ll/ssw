sswApp.controller("headerCtrl", ["$scope", "$state", "$mdDialog", "user", "$http", function($scope, $state, $mdDialog, user, $http){
	$scope.headerClass=$state.current.name;

	$scope.user = user.getUser();

	$scope.topNoti = topNoti[0];


	$scope.isNew = function(){
		if($scope.topNoti){
			var now = new Date();
			var notiDate = new Date(
					$scope.topNoti.REGISTER_DATE.substring(0, 4),
					$scope.topNoti.REGISTER_DATE.substring(5, 7)*1-1,
					$scope.topNoti.REGISTER_DATE.substring(8, 10),
					$scope.topNoti.REGISTER_DATE.substring(11, 13),
					$scope.topNoti.REGISTER_DATE.substring(14, 16),
					$scope.topNoti.REGISTER_DATE.substring(17, 19)
					);
			var timeDiff = Math.abs(now.getTime() - notiDate.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
			if(diffDays > 6){
				return false;
			}else{
				return true;
			}
		}else{
			return false;
		}
	}

	$scope.login = function(ev){

		$mdDialog.show({
			controller:"loginCtrl",
			templateUrl:"/ssw/tmpl/login.html",
			targetEvent:ev
		}).then(function(){
		}, function(){
		});
	}
	$scope.logout = function(){
		$http({
			method:"POST", 
			url :"/admin/logout"
		}).then(function(res){
			if(res.data.result = "logout"){
				window.location.reload();
			}
		}, function(err){
			$mdDialog.alert()
			.parent(angular.element(document.body))
			.title('로그아웃에 실패했습니다.')
			.textContent('다시 시도해보세요.')
			.ok('확인')
		});
	};
	$scope.openMenu = function($mdOpenMenu, ev){
		$mdOpenMenu(ev);
	}
	$scope.uploadImg= function(ev){
		$mdDialog.show({
			controller:"imageUploadCtrl",
			templateUrl : "/ssw/tmpl/imageUpload.html",
			targetEvent:ev,
			parent: angular.element(document.body)
		}).then(function(list){
		}, function(){
		});
	};


}]);
sswApp.controller("loginCtrl", ["$scope", "$http", "$mdDialog", function($scope, $http, $mdDialog){
        $scope.admin={
                id : "",
                pw : ""
        }
	$scope.close = function(){
		$mdDialog.cancel();
	}
        $scope.login = function(form){
                console.log(form);
                if(form.$invalid){

                }else{
                        console.log($scope.admin);
                        $http({
                                method:"POST",
                                url:"/admin/login",
                                data: $scope.admin
                        }).then(function(res){
                                if(res.data.result == "success"){
					window.location.reload();
					
                                }else{

                                        $mdDialog.show(
                                                $mdDialog.alert()
                                                .parent(angular.element(document.body))
                                                .title('로그인 정보가 맞지 않습니다.')
                                                .textContent('아이디 또는 비밀번호를 다시 확인해주세요.')
                                                .ok('확인')
                                        );

                                }
                        }, function(err){
                        });
                }
        }
}]);


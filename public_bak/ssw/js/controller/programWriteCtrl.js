sswApp.controller("programWriteCtrl", ["$scope", "$http", "$mdDialog", function($scope, $http, $mdDialog){

	$scope.tab = 1
	$scope.showTab = function (tabIdx) {
		$scope.tab = tabIdx;
		$scope.req($scope.tab)
	}

	$scope.programTitle = [
		 "구분",
		 "요일",
		 "시간",
		 "기간",
		 "강습료"
	];
	$scope.memberTitle = [
		"구분",
		"성인",
		"학생"
	];


	$scope.specialTitle= [
		"구분",
		"옵션",
		"가격"
	];

	$scope.comments = {};
	$scope.programs = {};
	$scope.members= {};
	$scope.specials= {};

	$scope.req= function(idx){
		$http({
			method:"GET",
			url : "/program/lec?branch="+idx
		}).then(function(data, status){
			$scope.programs.list= data.data;
		}, function(err, status){
		});
		$http({
			method:"GET",
			url : "/program/member?branch="+idx
		}).then(function(data, status){
			$scope.members.list = data.data;
		}, function(err, status){
		});
		$http({
			method:"GET",
			url : "/program/special?branch="+idx
		}).then(function(data, status){
			$scope.specials.list = data.data;
		}, function(err, status){
		});

		$http({
			method:"GET",
			url : "/program/comments?branch="+idx
		}).then(function(data, status){
			data = data.data;

			$scope.comments.lecture = data.lecture;
			$scope.comments.member = data.member;
			$scope.comments.special= data.special;
		}, function(err, status){
		});

	}
	$scope.programs.del  = function(num, idx, ev){
		var c = $mdDialog.confirm()
			.title('삭제하시겠습니까?')
			.textContent('선택한 강습을 삭제하시겠습니까?')
			.ariaLabel('del lecture')
			.targetEvent(ev)
			.ok('삭제')
			.cancel('취소');
		$mdDialog.show(c).then(function() {
			$http({
				method:"DELETE",
				url :"/admin/program/lec/"+num
			}).then(function(data, status){
				if(data.data.result == "success"){
					$scope.programs.list.splice(idx, 1);
				}
			}, function(err, status){
			});

		}, function() {
		});
	};
	$scope.programs.mod = function(num, idx, ev){
		$mdDialog.show({
			controller:"lectureCtrl",
			templateUrl :"/ssw/tmpl/modProgram/modLecture.html",
			targetEvent : ev,
			locals :{
				lecture:$scope.programs.list[idx],
				opt : "mod",
				branch: $scope.tab
			}
		}).then(function(data){
			$scope.programs.list[idx] = data;

		}, function(){
		});
	};
	$scope.programs.add = function(ev){
		$mdDialog.show({
			controller:"lectureCtrl",
			templateUrl :"/ssw/tmpl/modProgram/modLecture.html",
			targetEvent : ev,
			locals :{
				lecture : {},
				opt : "add",
				branch: $scope.tab
			}
		}).then(function(data){
			$scope.programs.list.push(data);

		}, function(){
		});
	};
	$scope.members.del= function(num, idx, ev){
		var c = $mdDialog.confirm()
			.title('삭제하시겠습니까?')
			.textContent('선택한 회원권을 삭제하시겠습니까?')
			.ariaLabel('del membership')
			.targetEvent(ev)
			.ok('삭제')
			.cancel('취소');
		$mdDialog.show(c).then(function() {
			$http({
				method:"DELETE",
				url :"/admin/program/member/"+num
			}).then(function(data, status){
				if(data.data.result == "success"){
					$scope.members.list.splice(idx, 1);
				}
			}, function(err, status){
			});

		}, function() {
		});
	};
	$scope.members.mod = function(num, idx, ev){
		$mdDialog.show({
			controller:"membershipCtrl",
			templateUrl :"/ssw/tmpl/modProgram/modMembership.html",
			targetEvent : ev,
			locals :{
				membership:$scope.members.list[idx],
				opt : "mod",
				branch: $scope.tab
			}
		}).then(function(data){
			$scope.members.list[idx] = data;

		}, function(){
		});
	};
	$scope.members.add= function(ev){
		$mdDialog.show({
			controller:"membershipCtrl",
			templateUrl :"/ssw/tmpl/modProgram/modMembership.html",
			targetEvent : ev,
			locals :{
				membership: {},
				opt : "add",
				branch: $scope.tab
			}
		}).then(function(data){
			$scope.members.list.push(data);

		}, function(){
		});
	};
	$scope.specials.del= function(num, idx, ev){
		var c = $mdDialog.confirm()
			.title('삭제하시겠습니까?')
			.textContent('선택한 특별가를 삭제하시겠습니까?')
			.ariaLabel('del specials')
			.targetEvent(ev)
			.ok('삭제')
			.cancel('취소');
		$mdDialog.show(c).then(function() {
			$http({
				method:"DELETE",
				url :"/admin/program/special/"+num
			}).then(function(data, status){
				if(data.data.result == "success"){
					$scope.specials.list.splice(idx, 1);
				}
			}, function(err, status){
			});

		}, function() {
		});
	};
	$scope.specials.mod = function(num, idx, ev){
		$mdDialog.show({
			controller:"specialCtrl",
			templateUrl :"/ssw/tmpl/modProgram/modSpecial.html",
			targetEvent : ev,
			locals :{
				special:$scope.specials.list[idx],
				opt : "mod",
				branch: $scope.tab
			}
		}).then(function(data){
			$scope.specials.list[idx] = data;

		}, function(){
		});
	};
	$scope.specials.add= function(ev){
		$mdDialog.show({
			controller:"specialCtrl",
			templateUrl :"/ssw/tmpl/modProgram/modSpecial.html",
			targetEvent : ev,
			locals :{
				special: {},
				opt : "add",
				branch: $scope.tab
			}
		}).then(function(data){
			$scope.specials.list.push(data);

		}, function(){
		});
	};
	$scope.comments.mod = function(ev, comment){
		$mdDialog.show({
			controller:"commentCtrl",
			templateUrl :"/ssw/tmpl/modProgram/modComment.html",
			targetEvent : ev,
			locals :{
				comment: comment,
				branch: $scope.tab
			}
		}).then(function(data){
			//$scope.specials.list.push(data);
			$scope.comments[comment.prg_key]=data;

		}, function(){
		});
	};

	$scope.openMenu = function($mdOpenMenu, ev){
		$mdOpenMenu(ev);
	}


	$scope.req(1);

}]);
sswApp.controller("lectureCtrl" , ["$scope", "$http", "$mdDialog", "lecture", "opt", "branch", function($scope, $http, $mdDialog, lecture, opt, branch){
	$scope.lecture = angular.copy(lecture);
	$scope.ui = {};
	if(opt == "mod"){
		$scope.ui.title ="강의 수정";
	}else if(opt == "add"){
		$scope.ui.title ="강의 추가";
	}
	$scope.close = function(){
		$mdDialog.cancel();
	}
	$scope.submit = function(form){
		if(!form.$invalid){
			for(var key in $scope.lecture){
				if($scope.lecture[key]==""){
					$scope.lecture[key] = "-";
				}
			}
			if(opt == "mod"){
				$http({
					method:"PUT",
					url:"/admin/program/lec/"+$scope.lecture.num,
					data : $scope.lecture
				}).then(function(data, status){
					$mdDialog.hide($scope.lecture);
				}, function(err, status){
					console.log(err);
				});
			}else if(opt == "add"){
				$scope.lecture.branch = branch;
				$http({
					method:"POST",
					url:"/admin/program/lec",
					data : $scope.lecture
				}).then(function(data, status){
					var newNum = data.data.newNum;
					$scope.lecture.num = newNum;
					$mdDialog.hide($scope.lecture);
				}, function(err, status){
					console.log(err);
				});
			}
		}
	}
}]);
sswApp.controller("membershipCtrl" , ["$scope", "$http", "$mdDialog", "membership", "opt", "branch", function($scope, $http, $mdDialog, membership, opt, branch){
	$scope.membership= angular.copy(membership);
	$scope.ui = {};
	if(opt == "mod"){
		$scope.ui.title ="회원권 수정";
	}else if(opt == "add"){
		$scope.ui.title ="회원권 추가";
	}
	$scope.close = function(){
		$mdDialog.cancel();
	}
	$scope.submit = function(form){
		if(!form.$invalid){
			for(var key in $scope.membership){
				if($scope.membership[key]==""){
					$scope.membership[key] = "-";
				}
			}
			if(opt == "mod"){
				$http({
					method:"PUT",
					url:"/admin/program/member/"+$scope.membership.num,
					data : $scope.membership
				}).then(function(data, status){
					$mdDialog.hide($scope.membership);
				}, function(err, status){
					console.log(err);
				});
			}else if(opt == "add"){
				$scope.membership.branch = branch;
				$http({
					method:"POST",
					url:"/admin/program/member",
					data : $scope.membership
				}).then(function(data, status){
					var newNum = data.data.newNum;
					$scope.membership.num = newNum;
					$mdDialog.hide($scope.membership);
				}, function(err, status){
					console.log(err);
				});
			}
		}
	}
}]);
sswApp.controller("specialCtrl" , ["$scope", "$http", "$mdDialog", "special", "opt", "branch", function($scope, $http, $mdDialog, special, opt, branch){
	$scope.special= angular.copy(special);
	$scope.ui = {};
	if(opt == "mod"){
		$scope.ui.title ="특별가 수정";
	}else if(opt == "add"){
		$scope.ui.title ="특별가 추가";
	}
	$scope.close = function(){
		$mdDialog.cancel();
	}
	$scope.submit = function(form){
		if(!form.$invalid){
			for(var key in $scope.special){
				if($scope.special[key]==""){
					$scope.special[key] = "-";
				}
			}
			if(opt == "mod"){
				$http({
					method:"PUT",
					url:"/admin/program/special/"+$scope.special.num,
					data : $scope.special
				}).then(function(data, status){
					$mdDialog.hide($scope.special);
				}, function(err, status){
					console.log(err);
				});
			}else if(opt == "add"){
				$scope.special.branch = branch;
				$http({
					method:"POST",
					url:"/admin/program/special",
					data : $scope.special
				}).then(function(data, status){
					var newNum = data.data.newNum;
					$scope.special.num = newNum;
					$mdDialog.hide($scope.special);
				}, function(err, status){
					console.log(err);
				});
			}
		}
	}
}]);
sswApp.controller("commentCtrl" , ["$scope", "$http", "$mdDialog", "comment", function($scope, $http, $mdDialog, comment){
	$scope.comment= angular.copy(comment);
	$scope.close = function(){
		$mdDialog.cancel();
	}
	$scope.submit = function(form){
		if(!form.$invalid){
			$http({
				method:"PUT",
				url:"/admin/program/comment/"+$scope.comment.num,
				data : $scope.comment
			}).then(function(data, status){
				$mdDialog.hide($scope.comment);
			}, function(err, status){
				console.log(err);
			});
		}
	}
}]);

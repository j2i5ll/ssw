sswApp.controller("staffWriteCtrl", ["$scope", "$http", "$mdDialog", function($scope, $http, $mdDialog){

	$scope.staffList = [];
	$http({
		url :"/staff",
		method:"GET"
	}).success(function(data){
		$scope.staffList = data;
	});

	$scope.addStaff = function(ev){
		$mdDialog.show({
			controller:"staffAddCtrl",
			templateUrl :"/ssw/tmpl/modStaff/modStaff.html",
			targetEvent : ev,
		}).then(function(data){
			//$scope.programs.list[idx] = data;
			console.log(data);
			$scope.staffList.push(data);
		}, function(){
		});
	}
	$scope.modStaff = function(staff, idx, ev){
		$mdDialog.show({
			controller:"staffModCtrl",
			templateUrl :"/ssw/tmpl/modStaff/modStaff.html",
			targetEvent : ev,
			locals :{
				staff:staff,
			}
		}).then(function(data){
			//$scope.programs.list[idx] = data;
			
			$scope.staffList[idx]  = data;
			//staff = data;

		}, function(){
		});
	}
	$scope.delStaff = function(num, idx, ev){
		var c = $mdDialog.confirm()
			.title('삭제하시겠습니까?')
			.textContent('선택한 강사/스태프를 삭제하시겠습니까?')
			.ariaLabel('del Staff')
			.targetEvent(ev)
			.ok('삭제')
			.cancel('취소');
		$mdDialog.show(c).then(function() {
			$http({
				method:"DELETE",
				url :"/admin/staff/"+num
			}).then(function(data, status){
				if(data.data.result == "success"){
					$scope.staffList.splice(idx, 1);
				}
			}, function(err, status){
			});

		}, function() {
		});
	}


}]);
sswApp.controller("staffModCtrl", ["$scope", "staff", "$mdDialog", "FileUploader", "$http", function($scope, staff, $mdDialog, FileUploader, $http){
        $scope.uploader = new FileUploader({
                url:"/admin/staff/photo",
		queueLimit : 1
        });
        $scope.uploader.filters.push({
                name:"extFilter",
                fn : function(item){
			var  name = item.name.toLowerCase();
			var ext = name.split(".").pop();
                        if(ext == "png" || ext == "jpg"){
                                return true;
                        }else{
                                return false;
                        }
                }
        },{
		name : "sizeFilter",
		fn : function(item){
			if(item.size > 4*1024*1024 ){
				return false;
			}else{
				return true;
			}
		}
	});
        $scope.uploader.onAfterAddingFile = function(fileItem) {
		$scope.uploader.uploadAll();
        };
        $scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
		fileItem.inProgress= false;
		$scope.staff.photo_path = "/profileImg/"+response.photo_path;
		$scope.uploader.clearQueue();
		document.querySelector("input[type=file]").value="";
        };



	$scope.staff = angular.copy(staff);
	$scope.staffModal={
		title:"강사 수정"
	};
	$scope.close = function(){
		$mdDialog.cancel();
	}
	$scope.submit = function(){
		$http({
			method:"PUT",
			url : "/admin/staff/"+$scope.staff.num,
			data : $scope.staff
		}).success(function(res){
			$mdDialog.hide(res.data);
		});
	}
}]);
sswApp.controller("staffAddCtrl", ["$scope", "$mdDialog", "$http", "FileUploader", function($scope, $mdDialog, $http, FileUploader){
        $scope.uploader = new FileUploader({
                url:"/admin/staff/photo",
		queueLimit : 1
        });
        $scope.uploader.filters.push({
                name:"extFilter",
                fn : function(item){
			var  name = item.name.toLowerCase();
			var ext = name.split(".").pop();
                        if(ext == "png" || ext == "jpg"){
                                return true;
                        }else{
                                return false;
                        }
                }
        },{
		name : "sizeFilter",
		fn : function(item){
			if(item.size > 4*1024*1024 ){
				return false;
			}else{
				return true;
			}
		}
	});
        $scope.uploader.onAfterAddingFile = function(fileItem) {
		$scope.uploader.uploadAll();
        };
        $scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
		fileItem.inProgress= false;
		$scope.staff.photo_path = "/profileImg/"+response.photo_path;
		$scope.uploader.clearQueue();
		document.querySelector("input[type=file]").value="";
        };





	$scope.staff = {
		photo_path:"/ssw/img/profile_placeholder.svg"
	};
	$scope.staffModal={
		title:"강사 추가"
	};
	$scope.close = function(){
		$mdDialog.cancel();
	}
	$scope.submit = function(){
		$http({
			method:"POST",
			url : "/admin/staff",
			data : $scope.staff

		}).success(function(res){
			$mdDialog.hide(res.data);
		});

	}
}]);

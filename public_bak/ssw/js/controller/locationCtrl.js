sswApp.controller("locationCtrl", ["$scope", function($scope){
	
	var lat = 37.402120, lng= 127.107019;
	var markers = [
		{
			position: new daum.maps.LatLng(lat, lng),
			text: '손상원클라이밍짐'
		}
	];

	var staticMapContainer  = document.getElementById('map'),
	staticMapOption = {
	center: new daum.maps.LatLng(lat, lng),
	level: 3,
	marker: markers
	};
	var staticMap = new daum.maps.StaticMap(staticMapContainer, staticMapOption);

}]);

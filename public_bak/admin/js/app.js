var sswAdminApp = angular.module("sswAdminApp", ["ngMessages", "ng", "ngMaterial"]);

sswAdminApp.controller("loginCtrl", ["$scope", "$http", "$mdDialog", function($scope, $http, $mdDialog){
	$scope.admin={
		id : "",
		pw : ""
	}
	$scope.login = function(form){
		console.log(form);
		if(form.$invalid){

		}else{
			console.log($scope.admin);
			$http({
				method:"POST",
				url:"/login",
				data: $scope.admin
			}).then(function(res){
				if(res.data.result == "success"){
					window.location.href="/";

				}else{
					
					$mdDialog.show(
						$mdDialog.alert()
						.parent(angular.element(document.body))
						.title('로그인 정보가 맞지 않습니다.')
						.textContent('아이디 또는 비밀번호를 다시 확인해주세요.')
						.ok('확인')
					);

				}
			}, function(err){
			});
		}
	}
}]);

